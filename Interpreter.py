import sys

import AST
from Exceptions import *
from Memory import *
from Memory import MemoryStack
from visit import *

sys.setrecursionlimit(10000)
from operator import add, sub, mul, div, gt, lt, ge, le, eq, ne, mod, lshift, rshift, or_, and_, xor


class Interpreter(object):
    def __init__(self):
        self.global_memory = MemoryStack("global")
        self.function_memory = MemoryStack("function")
        self.in_local_scope = False

    @on('node')
    def visit(self, node):
        pass

    @when(AST.BinExpr)
    def visit(self, node):
        left = node.left.accept(self)
        right = node.right.accept(self)
        op = node.op
        bin_ops = {
            '+': add,
            '-': sub,
            '*': mul,
            '/': div,
            '%': mod,
            '<<': lshift,
            '>>': rshift,
            '|': or_,
            '&': and_,
            '^': xor,
            '>': gt,
            '<': lt,
            '>=': ge,
            '<=': le,
            '==': eq,
            '!=': ne,
            '||': lambda l, r: l or r,
            '&&': lambda l, r: l and r,
        }
        return bin_ops[op](left, right)

    @when(AST.Const)
    def visit(self, node):
        return node.value

    @when(AST.WhileInstr)
    def visit(self, node):
        while node.condition.accept(self):
            try:
                node.instruction.accept(self)
            except BreakException:
                break
            except ContinueException:
                continue

    @when(AST.Integer)
    def visit(self, node):
        return int(node.value)

    @when(AST.Float)
    def visit(self, node):
        return float(node.value)

    @when(AST.String)
    def visit(self, node):
        return str(node.value)

    @when(AST.CodeBlocks)
    def visit(self, node):
        for child in node.children:
            child.accept(self)

    @when(AST.Args)
    def visit(self, node):
        return [arg.accept(self) for arg in node.args]

    @when(AST.Arg)
    def visit(self, node):
        return node.id

    @when(AST.Declarations)
    def visit(self, node):
        return [d.accept(self) for d in node.declarations]

    @when(AST.Declaration)
    def visit(self, node):
        return node.inits.accept(self)

    @when(AST.Assignment)
    def visit(self, node):
        assignment = node.expression.accept(self)
        if self.in_local_scope:
            if not self.function_memory.set(node.id, assignment):
                self.global_memory.set(node.id, assignment)
        else:
            self.global_memory.set(node.id, assignment)

    @when(AST.ChoiceInstr)
    def visit(self, node):
        if node.condition.accept(self):
            node.instruction.accept(self)
        elif node.else_instruction is not None:
            node.else_instruction.accept(self)

    @when(AST.ReturnInstr)
    def visit(self, node):
        raise ReturnValueException(node.expression.accept(self))

    @when(AST.ContinueInstr)
    def visit(self, node):
        raise ContinueException()

    @when(AST.BreakInstr)
    def visit(self, node):
        raise BreakException()

    @when(AST.RepeatInstr)
    def visit(self, node):
        while True:
            try:
                node.instructions.accept(self)
                if node.condition.accept(self):
                    break
            except ContinueException:
                if node.condition.accept(self):
                    break
            except BreakException:
                break

    @when(AST.FunDef)
    def visit(self, node):
        self.global_memory.insert(node.id, node)

    @when(AST.FunctionInvokation)
    def visit(self, node):
        was_in_local = self.in_local_scope
        name = node.id
        arguments = node.args.accept(self) if node.args is not None else list()

        # definition
        fun_def = self.global_memory.get(name)
        arguments_def = fun_def.args.accept(self) if fun_def.args is not None else []

        fun_mem = Memory("function call: " + name)

        if arguments:
            for index, value in enumerate(arguments):
                argument_def = arguments_def[index]
                fun_mem.put(argument_def, value)

        self.function_memory.push(fun_mem)
        self.in_local_scope = True

        try:
            fun_def.instructions.accept(self)
        except ReturnValueException as returnExpr:
            return returnExpr.value
        finally:
            self.function_memory.pop()
            self.in_local_scope = was_in_local

    @when(AST.PrintInstruction)
    def visit(self, node):
        for expr in node.expr_list.accept(self):
            print expr

    @when(AST.ExpressionList)
    def visit(self, node):
        return [expr.accept(self) for expr in node.expressions]

    @when(AST.Compound)
    def visit(self, node):
        compound_mem = Memory("compound_mem")
        was_in_local = self.in_local_scope
        self.in_local_scope = True
        res = []
        self.function_memory.push(compound_mem)
        try:
            if node.declarations is not None:
                for d in node.declarations.accept(self):
                    res.append(d)
            if node.instructions_opt is not None:
                for i in node.instructions_opt.accept(self):
                    res.append(i)
        except ReturnValueException as e:
            self.in_local_scope = was_in_local
            self.function_memory.pop()
            raise e

        self.in_local_scope = was_in_local
        self.function_memory.pop()
        return res

    @when(AST.LabeledInstruction)
    def visit(self, node):
        return node.instruction.accept(self)

    @when(AST.Instructions)
    def visit(self, node):
        return [i.accept(self) for i in node.instructions]

    @when(AST.Inits)
    def visit(self, node):
        return [init.accept(self) for init in node.inits]

    @when(AST.Init)
    def visit(self, node):
        result = node.expression.accept(self)
        if self.in_local_scope:
            self.function_memory.insert(node.id, result)
        else:
            self.global_memory.insert(node.id, result)

        return result

    @when(AST.Variable)
    def visit(self, node):
        if self.in_local_scope:
            result = self.function_memory.get(node.id)
            if result is None:
                result = self.global_memory.get(node.id)
        else:
            result = self.global_memory.get(node.id)

        return result
