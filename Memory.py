class Memory:
    def __init__(self, name):
        self.name = name
        self.container = {}

    def has_key(self, name):
        return name in self.container

    def get(self, name):
        return self.container.get(name, "")

    def put(self, name, value):
        self.container[name] = value


class MemoryStack:
    def __init__(self, name='default'):  # initialize memory stack with memory <memory>
        self.memory_stack = []
        self.memory_stack.append(Memory(name))

    def get(self, name):  # gets from memory stack current value of variable <name>
        for memory in self.reversed():
            if memory.has_key(name):
                return memory.get(name)
            elif "fun" in memory.name:
                return None

    def insert(self, name, value):
        self.peek().put(name, value)

    def set(self, name, value):
        for memory in self.reversed():
            if memory.has_key(name):
                memory.put(name, value)
                return True
            elif "fun" in memory.name:
                return False

    def push(self, memory):
        self.memory_stack.append(memory)

    def pop(self):
        return self.memory_stack.pop()

    def peek(self):
        return self.memory_stack[-1]

    def reversed(self):
        return reversed(self.memory_stack)
